using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }


    public Transform player;
    public float score;
    public int lifes;
    // Start is called before the first frame update

    private void Awake()
    {
        if (_instance == null)
        {
            DontDestroyOnLoad(gameObject);
            _instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        score = 0;
        lifes = 2;
    }

    
    void Update()
    {
        addScore();
    }

    public void addScore()
    {
        if (player.position.y > score)
        {
            score = player.position.y;
        }
    }
}
