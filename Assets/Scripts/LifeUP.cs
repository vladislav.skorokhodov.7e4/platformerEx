using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeUP : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if(Player.lifes != 3)
            {
                Player.lifes++;
            }
            
            Destroy(gameObject);
        }
    }
}
