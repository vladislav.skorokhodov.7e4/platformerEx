using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public Transform player;
    public Vector3 offset;
    float highestPos = 0;
    void Update()
    {
        if (player.position.y > highestPos)
        {
            transform.position = new Vector3(0, player.position.y, offset.z);
            highestPos = player.position.y;
        }
    }
}
