using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lifes : MonoBehaviour
{
    public Text LifeCounterText;

    void Update()
    {
        float lifes = Player.lifes;
        LifeCounterText.text = "Lifes: " + lifes;
    }
}
