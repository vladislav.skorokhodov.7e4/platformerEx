using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public Text ScoreCounterText;
    public Text LifeCounterText;
    void Update()
    {

        ScoreCounterText.text = "Score: " + GameManager.Instance.score;
    }
}
