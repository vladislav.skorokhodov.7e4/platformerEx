using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCode : MonoBehaviour
{

    public Transform player;
    public float moveSpeed = 10f;
    public Rigidbody2D self;
    public float agroDistance;
    void Update()
    {
        float distToPlayer = Vector2.Distance(transform.position, player.position);

        if(distToPlayer < agroDistance)
        {
            if(transform.position.x < player.position.x)
            {
                self.velocity = new Vector2(moveSpeed, 0);
                transform.localScale = new Vector2(1, 1);
            }
            else 
            {
                self.velocity = new Vector2(-moveSpeed, 0);
                transform.localScale = new Vector2(-1, 1);
            }
        }

    }



    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Collisioning");
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("DEAD");
            GameObject pl = collision.gameObject;
            Destroy(gameObject);
            Player.lifes--;
        }
    }
}
