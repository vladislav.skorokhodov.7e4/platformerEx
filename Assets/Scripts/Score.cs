using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public Text ScoreCounterText;
    public Transform player;
    public float score;

    void Update()
    {
        if (player.position.y > score)
        {
            score = player.position.y;
        }
        ScoreCounterText.text = "Score: " + score * 100;
    }
}
