using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Rigidbody2D rigidbody2d;
    private BoxCollider2D boxCollider2d;
    [SerializeField] private LayerMask platforslayerMask;

    public float jumpVelocity = 10f;
    public int jumps = 2;
    private int actualJumps = 0;
    public float moveSpeed = 10f;

    public static int lifes = 2;
    public int maxLifes = 3;

    private bool IsGrounded = false;

    private float fireSpellStart = 0f;
    private float fireSpellCooldown = 2f;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (transform.position.y > collision.transform.position.y)
        {
            IsGrounded = true;
            actualJumps = jumps;
        }
    }

    private void PlayerMovement()
    {
        float xMove = 0.0f;
        //Movement
        if (Input.GetAxis("Horizontal")> 0)
        {
            transform.position += Time.deltaTime * moveSpeed * Vector3.right;
            transform.Rotate(Vector3.back * moveSpeed * 75 * Time.deltaTime);
            xMove = xMove + moveSpeed;
            if (Time.time > fireSpellStart + fireSpellCooldown && Input.GetKeyDown(KeyCode.C))
            {
                rigidbody2d.AddForce(new Vector2(10, 0), ForceMode2D.Impulse);
            }
        }
        else if (Input.GetAxis("Horizontal") < 0)
        {
            transform.position += Time.deltaTime * moveSpeed * Vector3.left;
            transform.Rotate(-Vector3.back * moveSpeed * 75 * Time.deltaTime);
            xMove = xMove - moveSpeed;
            if (Time.time > fireSpellStart + fireSpellCooldown && Input.GetKeyDown(KeyCode.C))
            {
                rigidbody2d.AddForce(new Vector2(-10, 0), ForceMode2D.Impulse);
            }
        }



        float tiltAroundZ = Input.GetAxis("Horizontal") * moveSpeed;
        float tiltAroundX = Input.GetAxis("Vertical") * moveSpeed;
        Quaternion target = Quaternion.Euler(tiltAroundX, 0, tiltAroundZ);
    }

    void Awake()
    {
        rigidbody2d = transform.GetComponent<Rigidbody2D>();
        boxCollider2d = transform.GetComponent<BoxCollider2D>();
    }

    void Update()
    {
        if (IsGrounded && Input.GetKeyDown(KeyCode.Space) && actualJumps > 0)
        {
            rigidbody2d.velocity = Vector2.up * jumpVelocity;
            actualJumps = actualJumps - 1;

        }
        PlayerMovement();

        if (lifes <= 0)
        {
            Destroy(gameObject);
        }

    }

}

